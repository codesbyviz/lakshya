import * as React from 'react';
import * as Native from 'react-native';

import { ActivityIndicator, Colors } from 'react-native-paper';

const styles = Native.StyleSheet.create({
    Container: {
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center",
        backgroundColor:"#FFFFFF",
    },
    ProgressBar:{
        marginTop: 20,
        height:10,
        width:"20%",

    },
    Logo:{
        width:100,
        height:120,
    },
  });
export default class SplashScreen extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            name:"",
        }
        this._bootstrapAsync();
    }
    _bootstrapAsync = async () => {
        const userToken = await Native.AsyncStorage.getItem('userToken');
        this.props.navigation.navigate(userToken ? 'App' : 'Auth');
    };
    render() {
        return (
            <Native.View style={styles.Container}>
                <Native.Image style={styles.Logo} source={require('src/assets/images/logo.png')} />
                <Native.ProgressBarAndroid color="#000000" style={styles.ProgressBar} styleAttr="Horizontal" />
            </Native.View>            
        )
    }
}
