import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import SplashScreen from './src/SplashScreen';
import HomeScreen from './src/App/Home';
import SignInScreen from './src/Auth/Login';
import Chats from './src/App/Chats';


const AppStack = createStackNavigator({ Home: HomeScreen});
const AuthStack = createStackNavigator(
  { 
    SignIn: {
      screen:SignInScreen,    
    },
    Chats: {
      screen:Chats,    
    }
  },
  {
    initialRouteName:"SignIn",
    defaultNavigationOptions: {
      header:null,
    },
    }
);

export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: SplashScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
));